/*
class Product{
    constructor(name,description,price,pic,onsale){
        this.name = name;
        this.description = description;
        this.price = price;
        this.pic = pic
        this.onsale= onsale
    }
}

var product1 = new Product("AMD Ryzen 9 5900X Desktop CPU","A formidable upgrade to your desktop system, the AMD Ryzen 9 5900X Desktop CPU offers powerful computing performance to handle heavy-load applications and multi-threaded tasks, such as content creation, video encoding, and gaming.",859,"images/pic1.jpg",true);
var product2 = new Product("Intel Core i7-10700k Boxed Processor","The Intel Core i7- 10700K Boxed Processor offers fast performance and advanced technologies, including Turbo Boost 2.0, Hyper-Threading, and Virtualization, making it a formidable upgrade for your compatible system or a great option for building a new setup.",539,"images/pic2.jpg",false);
var product3 = new Product("AMD Ryzen 9 5950X Desktop CPU","Keeping the pace with intensive multi-processing applications and hardcore video gaming, the AMD Ryzen 9 5950X Desktop CPU delivers excellent computing performance without sacrificing power-efficiency.",1249,"images/pic3.jpg",false);
var product4 = new Product("ADATA XPG Spectrix D60G 16GB Memory Kit","Satisfying e-sports players with that they need, the ADATA XPG Spectrix D60G DDR4 RGB 3600MHz 16GB Memory Kit (2x8GB) features a wide frequency range for fluid overclocking performance and a unique RGB strip to light up the battlefield.",209,"images/pic4.jpg",true);
var product5 = new Product("DDR4 3000MHz Vengeance LPX White","Vengeance LPX memory is designed for high-performance overclocking. The heatspreader is made of pure aluminum for faster heat dissipation, and the eight-layer PCB helps manage heat and provides superior overclocking headroom",219,"images/pic5.jpg",false);
var product6 = new Product("Team 16GB Elite 2666MHz DDR4 RAM","Team 16GB (1x16GB) TED416G2666C1902 Elite 2666MHz DDR4 RAM",99,"images/pic6.jpg",false);
var product7 = new Product("ADATA Ultimate SU650 120GB Internal SSD","The ADATA Ultimate SU650 Internal SSD delivers read and write speeds of up to 520 and 420 MB/s* respectively, making it perfect for relocating large files like Blu-ray movies, gaming software, surveillance footage, and more.",56,"images/pic7.jpg",true);
var product8 = new Product("WD 120G Green M.2 SSD","For fast performance and reliability, WD Green SSDs boost the everyday computing experience in your desktop or laptop PC.",30,"images/pic8.jpg",false);
var product9 = new Product("WD 240G Green M.2 SSD (G2 Version)","With the performance boost from a WD Green SATA SSD, you can browse the web, play a game, or simply start your system in a flash.",48,"images/pic9.jpg",false);
var product10 = new Product("Galax GeForce RTX 3070 1-Click OC 8G Graphics Card","For the OC series, we have an innovative fan design that creates a new trend in GPU cooling with the 102mm twin fans along with our proprietary Fan Blade - “Wings”. 11 fan blades being extraordinary, not only for the exquisite look but also providing high air flow and air pressure at minimum noise level.",1699,"images/pic10.jpg",false);
var product11 = new Product("Asus GeForce 2GB GDDR5 Video Card","ASUS GeForce GT 710 great value graphics with passive 0dB efficient cooling. Silent passive cooling means true 0dB - perfect for quiet home theatre PCs and multimedia centres. Industry only AUTO-EXTREME Technology, 100% full automation for best reliability.",69,"images/pic11.jpg",false);
var product12 = new Product("Gigabyte GeForce RTX 2060 R2.0 6G OC Graphics Card","GeForce RTX™ 2060 OC 6G(2.0)GeForce RTX™ 2060 OC 6G(2.0)GeForce RTX™ 2060 OC 6G(2.0)GeForce RTX™ 2060 OC(2.0)GeForce RTX™ 2060 OC(2.0)GeForce RTX™ 2060 OC(2.0)GeForce RTX™ 2060 OC(2.0)GeForce RTX™ 2060 OC(2.0)",799,"images/pic12.jpg",false);
var product13 = new Product("Asus Prime B450M-A mATX Motherboard","Fan Xpert 2+: Flexible controls for ultimate cooling and silence, plus GPU-temperature sensing for cooler gaming. Ultrafast connectivity: Supreme flexibility with USB 3.1 Gen 2 and native M.2. ASUS Aura Sync header: Onboard connector for RGB LED strips, easily synced with an ever-growing portfolio of Aura Sync-capable hardware.",105,"images/pic13.jpg",false);
var product14 = new Product("Asus Prime Z390M Plus Motherboard","OptiMem II: Careful routing of traces and vias, plus ground layer optimizations to preserve signal integrity for improved memory overclocking. Enhanced power solution: Premium components provide better power efficiency. Industry-leading cooling options: Comprehensive controls for fans and AIO pump, via Fan Xpert 4 or the acclaimed UEFI.",209,"images/pic14.jpg",false);
var product15 = new Product("","AM4 socket: Ready for 2nd and 3rd Gen AMD Ryzen processors to maximize connectivity and speed with up to two M.2 Drives, USB 3.2 Gen2 and AMD StoreMI. Aura Sync RGB: ASUS-exclusive Aura Sync RGB lighting, including RGB headers and addressable Gen 2 GRB headers. Optimal Power Solution: 12+4 power stages with ProCool II power connector, alloy chokes and durable capacitors to support multi-core processors.",415,"images/pic15.jpg",false);
var productlist = [product1,product2,product3,product4,product5,product6,product7,product8,product9,product10,product11,product12,product13,product14,product15]
var taglist = ["tag1","tag2","tag3","tag4","tag5","tag6","tag7","tag8","tag9","tag10","tag11","tag12","tag13","tag14","tag15"]
var taglistname = ["tag1name","tag2name","tag3name","tag4name","tag5name","tag6name","tag7name","tag8name","tag9name","tag10name","tag11name","tag12name","tag13name","tag14name","tag15name"]
var piclist = ["pic1","pic2","pic3","pic4","pic5","pic6","pic7","pic8","pic9","pic10","pic11","pic12","pic13","pic14","pic15"]

function index()
{
    for(var i=0; productlist.length;i++){
    document.getElementById(taglist[i]).innerHTML=
    
    "<p>"+productlist[i].description+"</p><br>"+
    "<p>$"+productlist[i].price+"</p><br>";
    document.getElementById(taglistname[i]).innerHTML=
    "<h3>"+productlist[i].name+"</h3><br>";
    
    document.getElementById(piclist[i]).src=productlist[i].pic;
}
}

function on_sale()
{
    
    for(var i=0; productlist.length;i++){
      if(productlist[i].onsale==true){
        productprice=productlist[i].price*.9
        document.getElementById(taglist[i]).innerHTML=
        "<h3>"+productlist[i].name+"</h3><br>"+
        "<p>"+productlist[i].description+"</p><br>"+
        "<p>Special $"+Math.ceil(productprice)+"</p><br>";
        document.getElementById(piclist[i]).src=productlist[i].pic;
}
}
}

function formValidation() {
    var uname = document.registration.username;
    var uadd = document.registration.address;
    var uemail = document.registration.email;
    var ValidateDateofBirth = document.registration.dateofbirth;
    var ufsex = document.registration.fsex;
        if (allLetter(uname)) {
            if (alphanumeric(uadd)) {
                if (ValidateEmail(uemail)) {
                    if (allnumeric(uzip)) {
                        if (validsex(umsex, ufsex)) {}
                            }
                        }
                    }
                }
                return false;
            }





function allLetter(uname) {
    var letters = /^[A-Za-z]+$/;
    if (uname.value.match(letters)) {
        return true;
    }
    else {
        alert('Username must have alphabet characters only');
        uname.focus();
        return false;
    }
}

function alphanumeric(uadd) {
    var letters = /^[0-9a-zA-Z]+$/;
    if (uadd.value.match(letters)) {
        return true;
    }
    else {
        alert('User address must have alphanumeric characters only');
        uadd.focus();
        return false;
    }
}

function ValidateEmail(mail) {
    if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(myForm.emailAddr.value)) {
        return (true)
    }
    alert("You have entered an invalid email address!")
    return (false)
}

function ValidateDateofBirth (date) {
    var pattern = /^([0-9]{2})-([0-9]{2})-([0-9]{4})$/;
    if (pattern.test(str_input_date)) {
        alert("valid date");
        dateofbirth.focus()
        return false;
    }
    return true;

}

function allnumeric(uphone) {
    var numbers = /^[0-9]+$/;
    if (uphone.value <1 (uphone)) {
        return true;
    }
    else {
        alert ("Enter Country Code");
        uphone.focus();
        return false;
    }

}

function passid_validation(passid, mx, my) {
    var passid_len = passid.value.length;
    if (passid_len == 0 || passid_len >= my || passid_len < mx) {
        alert("Password should not be empty / length be between " + mx + " to " + my);
        passid.focus();
        return false;
    }
    return true;
}
*/





function validateInput() {

    var name = document.getElementById("name").value;
    var address = document.getElementById("address").value;
    var email = document.getElementById("email").value;
    var password = document.getElementById("password").value;
    var phone = document.getElementById("phone").value;
    




    var birthdate = document.getElementById("txtDate").value;

   


    if (name.toString().length == 0) {
        alert("Please enter your name")

        return false;


    }
    else if (address.toString().length == 0) {
        alert("Please enter your address")
        return false;
    }
    else if (email.toString().length == 0) {
        alert("Please enter your email")
        return false;
    }
    else if (password.toString().length == 0) {
        alert("Please enter your password")
        return false;
    }
    else if (phone.toString().length == 0) {
        alert("Please enter your contact nuber")
        return false;

    }
    else if (birthdate.toString().length == 0) {
        alert("Please enter your birthdate")
        return false;
    }


    else {
        return true;
    }
}



function phonenumber(inputtxt)
{
var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
if(inputtxt.value.match(phoneno)){


return true;
}
else
{
alert("In contact number please include country code with '+XX-XXXX-XXXX' format");
return false;
}
}













 var removeItemBtn = document.getElementsByClassName('btn btn-outline-danger')
 console.log(removeItemBtn)
 for (var i=0; i<removeItemBtn.length;i++){
     var button = removeItemBtn[i]
     button.addEventListener('click',removeCartItem)
 }

 var quantityInputs = document.getElementsByClassName('quantity-input')
    for (var i = 0; i < quantityInputs.length; i++) {
        var input = quantityInputs[i]
        input.addEventListener('change', quantityChanged)
    }

function quantityChanged(event) {
    var input = event.target
    if (isNaN(input.value) || input.value <= 0) {
        input.value = 1
    }
    updateCartTotal()
}

function removeCartItem(event) {
    var buttonClicked = event.target
    buttonClicked.parentElement.parentElement.parentElement.parentElement.parentElement.remove()
    updateCartTotal()
}



var addToCartButtons = document.getElementsByClassName('btn btn-success')
    for (var i = 0; i < addToCartButtons.length; i++) {
        var button = addToCartButtons[i]
        button.addEventListener('click', addToCartClicked)
    }

function addToCartClicked(event) {
    var button = event.target
    var shopItem = button.parentElement.parentElement.parentElement
    var title = shopItem.getElementsByClassName('card-title')[0].innerText
    
    var price = shopItem.getElementsByClassName('shop-item-price')[0].innerText
    var imageSrc = shopItem.getElementsByClassName('shop-item-image')[0].src
    console.log(title,price,imageSrc)
    addItemToCart(title, price, imageSrc)
    updateCartTotal()
}

 function updateCartTotal(){
     var cartItemContainer = document.getElementsByClassName('cart-row')[0]
     var cartRows = cartItemContainer.getElementsByClassName('row')
     console.log(cartRows)
     var total = 0
     var temp = 0
     for (var i=0; i<cartRows.length;i++){
         var cartRow = cartRows[i]
         var priceElement = cartRow.getElementsByClassName('price')[0]
         var quantityElement = cartRow.getElementsByClassName('quantity-input')[0]
         var price = parseFloat(priceElement.innerText.replace('$',' '))
         var quantity = quantityElement.value
         temp = (price * quantity)
         total+=temp/2
     }
     
     total = Math.round(total * 100) / 100
     document.getElementsByClassName('textOrderTotal')[0].innerText=total
 }

 function addItemToCart(title, price, imageSrc) {
    var cartRow = document.createElement('div')
    cartRow.classList.add('cart-row')
    var cartItems = document.getElementsByClassName('cart-row')[0]
    var cartRowContents = `
    <div class="card-body">

        <div class="row">
            <div class="d-none d-lg-block col-lg-2 text-center py-2">
                <img src="${imageSrc}" width="120" height="80" />
            </div>
            <div class="col-12 text-sm-center col-lg-5 text-lg-left">
                <h4><strong>${title}</strong></h4>
                <h4><small></small></h4>

            </div>
            <div class="price col-12 text-sm-center col-lg-5 text-lg-right row">
                
                <div class="col-4 text-md-right sm-2" style="padding-top:5px;">
                    <div class="price">
                    <h6><strong>$${price}<span class="text-muted"></span></strong></h6>
                </div>
                </div>
                <div class="col-6 text-md-right sm-2" style="padding-top:5px;">
                    
                    <input class="quantity-input" type="number" value="1">
                    
                </div>
                
                <div class="col-2 col-sm-6 col-lg-2 text-right">
                    <div class="float-right mx-1">
                        <button type="submit" class="btn btn-outline-danger"  >
                            <i class="fas fa-trash"></i>
                        </button>

                    </div>

                </div>

            </div>

        </div>
        
        </div>`
        cartRow.innerHTML = cartRowContents
    cartItems.append(cartRow)
    cartRow.getElementsByClassName('btn btn-outline-danger')[0].addEventListener('click', removeCartItem)
    cartRow.getElementsByClassName('quantity-input')[0].addEventListener('change', quantityChanged)
 }
